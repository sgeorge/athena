/*
 *   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#ifndef ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONAUXCONTAINER_H
#define ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONAUXCONTAINER_H

#include "AthenaPoolExampleData/versions/ExampleElectronAuxContainer_v1.h"

namespace xAOD {
typedef ExampleElectronAuxContainer_v1 ExampleElectronAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::ExampleElectronAuxContainer, 1202247486, 1);

#endif
