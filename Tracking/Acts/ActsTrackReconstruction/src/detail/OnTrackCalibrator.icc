/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "ActsGeometry/SurfaceOfMeasurementUtil.h"
#include "ActsGeometry/ATLASSourceLink.h"

namespace ActsTrk::detail {
  
  template <typename traj_t>
  OnTrackCalibrator<traj_t> OnTrackCalibrator<traj_t>::NoCalibration(const Acts::TrackingGeometry &trackingGeometry,
								     const ActsTrk::DetectorElementToActsGeometryIdMap &detectorElementToGeoId)
  {
    ToolHandle<IOnTrackCalibratorTool<traj_t>> null(nullptr);
    return OnTrackCalibrator<traj_t>(trackingGeometry, detectorElementToGeoId, null, null, null);
  }
  
  template <typename traj_t>
  OnTrackCalibrator<traj_t>::OnTrackCalibrator(const Acts::TrackingGeometry &trackingGeometry,
					       const ActsTrk::DetectorElementToActsGeometryIdMap &detectorElementToGeoId,
					       const ToolHandle<IOnTrackCalibratorTool<traj_t>>& pixelTool,
					       const ToolHandle<IOnTrackCalibratorTool<traj_t>>& stripTool,
					       const ToolHandle<IOnTrackCalibratorTool<traj_t>>& hgtdTool)
    : m_trackingGeometry(&trackingGeometry),
      m_detectorElementToGeoId(&detectorElementToGeoId)
  {
    if (pixelTool.isEnabled()) {
      pixelTool->connect(*this);
    } else {
      pixelCalibrator.template connect<&OnTrackCalibrator<traj_t>::passthrough<2, xAOD::PixelCluster>>(this);
    }
    
    if (stripTool.isEnabled()) {
      stripTool->connect(*this);
    } else {
      stripCalibrator.template connect<&OnTrackCalibrator<traj_t>::passthrough<1, xAOD::StripCluster>>(this);
    }
    if (hgtdTool.isEnabled()) {
      hgtdTool->connect(*this);
    } else {
	// cppcheck-suppress missingReturn; false positive
        hgtdCalibrator.template connect<&OnTrackCalibrator<traj_t>::passthrough<3, xAOD::HGTDCluster>>(this);
    }
  }
  
  template <typename traj_t>
  void OnTrackCalibrator<traj_t>::calibrate(
					    const Acts::GeometryContext& geoctx,
					    const Acts::CalibrationContext& cctx,
					    const Acts::SourceLink& link,
					    TrackStateProxy state) const
  {
    state.setUncalibratedSourceLink(Acts::SourceLink{link});
    ATLASUncalibSourceLink sourceLink = link.template get<ATLASUncalibSourceLink>();
    assert(sourceLink!=nullptr);
    const xAOD::UncalibratedMeasurement &measurement = getUncalibratedMeasurement(sourceLink);
    // @TODO in principle only need to lookup surface for strips
    const Acts::Surface *surface = getSurfaceOfMeasurement(*m_trackingGeometry, *m_detectorElementToGeoId, measurement);
    if (!surface) throw std::runtime_error("No surface for measurement.");
    switch (measurement.type()) {
    case xAOD::UncalibMeasType::PixelClusterType: {
      assert(pixelCalibrator.connected());
      auto [pos, cov] = pixelCalibrator(geoctx,
					cctx,
					*dynamic_cast<const xAOD::PixelCluster*>(&measurement),
					state);
      setState<2>(xAOD::UncalibMeasType::PixelClusterType,
		  pos,
		  cov,
		  surface->bounds().type(),
		  state);
      break;
    }
    case xAOD::UncalibMeasType::StripClusterType: {
      assert(stripCalibrator.connected());
      auto [pos, cov] = stripCalibrator(geoctx,
					cctx,
					*dynamic_cast<const xAOD::StripCluster*>(&measurement),
					state);
      setState<1>(xAOD::UncalibMeasType::StripClusterType,
		  pos,
		  cov,
		  surface->bounds().type(),
		  state);
      break;
    }
    case xAOD::UncalibMeasType::HGTDClusterType: {
      assert(hgtdCalibrator.connected());
      auto [pos, cov] = hgtdCalibrator(
        geoctx,
        cctx,
	*dynamic_cast<const xAOD::HGTDCluster*>(&measurement),
        state);
    setState<3>(
        xAOD::UncalibMeasType::HGTDClusterType,
        pos,
	cov,
	surface->bounds().type(),
        state);
    break;
    }
    default:
      throw std::domain_error("OnTrackCalibrator can only handle Pixel or Strip measurements");
    }
  }
  
  template <typename traj_t>
  template <std::size_t Dim, typename Cluster>
  std::pair<xAOD::MeasVector<Dim>, xAOD::MeasMatrix<Dim>>
  OnTrackCalibrator<traj_t>::passthrough(const Acts::GeometryContext& /*gctx*/,
					 const Acts::CalibrationContext& /*cctx*/,
					 const Cluster& cluster,
					 const TrackStateProxy& /*state*/) const
  {
    return std::make_pair(cluster.template localPosition<Dim>(),
			  cluster.template localCovariance<Dim>());
  }
  
} // namespace ActsTrk

