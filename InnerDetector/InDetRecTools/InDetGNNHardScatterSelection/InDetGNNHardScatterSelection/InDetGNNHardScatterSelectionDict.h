/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef InDetGNNHardScatterSelectionDict_h
#define InDetGNNHardScatterSelectionDict_h

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__ 

#include "InDetGNNHardScatterSelection/GNNTool.h"

#endif // InDetGNNHardScatterSelectionDict
