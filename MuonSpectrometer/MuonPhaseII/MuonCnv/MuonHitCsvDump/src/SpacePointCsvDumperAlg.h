#ifndef MUONCSVDUMP_MuonStripCsvDumperAlg_H
#define MUONCSVDUMP_MuonStripCsvDumperAlg_H
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include <AthenaBaseComps/AthAlgorithm.h>
#include <MuonIdHelpers/IMuonIdHelperSvc.h>
#include <StoreGate/ReadHandleKey.h>
#include <ActsGeometryInterfaces/ActsGeometryContext.h>
#include <MuonSpacePoint/SpacePointContainer.h>


/** The MuonStripCsvDumperAlg reads the RpcStripContainer and dumps information to csv files
 *  The files are used for the algorithm development in acts **/

namespace MuonR4{
class SpacePointCsvDumperAlg: public AthAlgorithm {

   public:

     SpacePointCsvDumperAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~SpacePointCsvDumperAlg() = default;

     StatusCode initialize() override;
     StatusCode execute() override;

   private:

    
    SG::ReadHandleKey<SpacePointContainer> m_readKey{this, "ReadKey", "MuonSpacePoints", "Key to the space point container"};

    ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc{this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};

    size_t m_event{0};

};
}
#endif