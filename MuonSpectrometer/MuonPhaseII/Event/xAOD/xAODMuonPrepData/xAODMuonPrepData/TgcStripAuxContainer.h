/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_TGCSTRIPAUXCONTAINER_H
#define XAODMUONPREPDATA_TGCSTRIPAUXCONTAINER_H

#include "xAODMuonPrepData/TgcStripFwd.h"
#include "xAODMuonPrepData/versions/TgcStripAuxContainer_v1.h"

// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TgcStripAuxContainer , 1242132729 , 1 )
#endif  // XAODMUONRDO_NTGCRDO_H
