# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( GdbUtils )

# Install files from the package:
atlas_install_python_modules( python/*.py )
